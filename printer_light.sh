#!/bin/bash


commands=("printer_on" "printer_off" "failed_print_anim")
CMD=""

if [ "${1}" == "a" ]; then
    CMD="${commands[$2]} 00 34"
else
    declare -A printer

    # Lulzbot Mini
    printer[0,0]="00"
    printer[0,1]="06"

    # Ultimaker
    printer[1,0]="07" 
    printer[1,1]="14"

    # Monoprice
    printer[2,0]="15" 
    printer[2,1]="22"

    # Lulzbot Taz
    printer[3,0]="23" 
    printer[3,1]="34"

    CMD="${commands[$2]} ${printer[$1,0]} ${printer[$1,1]}"
fi

echo "${CMD}"  | nc lumen1.local 5000